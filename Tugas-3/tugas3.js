//soal no.1
var kataPertama = "saya ";
var kataKedua = "senang ";
var kataKetiga = "belajar ";
var kataKeempat = "javascript";

var a = kataKedua.charAt(0).toUpperCase() + kataKedua.slice(1);
var b = kataKeempat.toUpperCase();

console.log(kataPertama.concat(a).concat(kataKetiga).concat(b));

//soal no.2
var kataPertama = Number("1");
var kataKedua = Number("2");
var kataKetiga = Number("4");
var kataKeempat = Number("5");

console.log(kataPertama+kataKedua+kataKetiga+kataKeempat);

//soal no.3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14); // do your own! 
var kataKetiga = kalimat.substring(15, 18); // do your own! 
var kataKeempat = kalimat.substring(19, 25); // do your own! 
var kataKelima = kalimat.substring(25, 32); // do your own! 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

//soal no.4
var nilai = 76;
if(nilai>=80){
    console.log('index = A')
}else if(nilai>=70 && nilai<80){
    console.log('index = B')
}else if(nilai>= 60 && nilai<70){
    console.log('index = C')
}else if(nilai>=50 && nilai<60){
    console.log('index = D')
}else if(nilai<50){
    console.log('index = E')
}

//soal no.5
var tanggal = '12';
var bulan = 8;
var tahun = '1998';

switch(bulan)
{
    case 1:
        {console.log(tanggal.concat('Januari ', tahun)); break;}
    case 2:
        {console.log(tanggal.concat('Februari ', tahun)); break;}
    case 3:
        {console.log(tanggal.concat('Maret ', tahun)); break;}
    case 4:
        {console.log(tanggal.concat('April ', tahun)); break;}
    case 5:
        {console.log(tanggal.concat('Mei ', tahun)); break;}
    case 6:
        {console.log(tanggal.concat('Juni ', tahun)); break;}
    case 7:
        {console.log(tanggal.concat('Juli ', tahun)); break;}
    case 8:
        {console.log(tanggal.concat('Agustus ', tahun)); break;}
    case 9:
        {console.log(tanggal.concat('September ', tahun)); break;}
    case 10:
        {console.log(tanggal.concat('Oktober ', tahun)); break;}
    case 11:
        {console.log(tanggal.concat('November ', tahun)); break;}
    case 12:
        {console.log(tanggal.concat('Desember ', tahun)); break;}
    default:
        {console.log('False');}

}

