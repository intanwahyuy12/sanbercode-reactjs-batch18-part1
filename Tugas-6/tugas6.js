//soal 1
const luasLingkaran = (myRadius) => {
    let area = (myRadius * myRadius * Math.PI)
    return area
}

const kelilingLingkaran = (myRadius) => {
    let area = (2 * Math.PI * myRadius)
    return area
}
console.log(luasLingkaran(6))
console.log(kelilingLingkaran(6))

//soal 2
let kalimat = ""
const sentences = (kata) => {
    let res = kalimat += kata
    return res 
}

sentences("saya ")
sentences("adalah ")
sentences("seorang ")
sentences("frontend ")
sentences("developer ")
console.log(kalimat)

//soal 3
const newFunction = function literal(firstName, lastName){
    return {
      firstName, lastName,
      fullName: function(){
        console.log(firstName + " " + lastName)
      }
    }
  }
   
//Driver Code 
newFunction("William", "Imoh").fullName() 

//soal 4
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

  const {firstName, lastName, destination, occupation, spell} = newObject
  console.log(firstName, lastName, destination, occupation)

  //soal 5
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = west.concat(east)
//Driver Code
let combinedArray = [...west, ...east]
console.log(combinedArray)