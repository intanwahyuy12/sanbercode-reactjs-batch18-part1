//soal 1
console.log('LOOPING PERTAMA');
for(var angka =2; angka<21; angka+=2){
    console.log(angka+ ' - I LOVE CODING');
}

console.log('LOOPING KEDUA');
for(var angka=20; angka>1; angka-=2){
    console.log(angka+ ' - I WILL BECOME A FRONTEND DEVELOPER');
}

//soal 2
for(var angka=1; angka < 21; angka++){
    if(angka %3==0 && angka %2 ==1){
        console.log(angka + ' - I Love Coding')
    }else if(angka %2 == 0){
        console.log(angka + ' - Berkualitas')
    }else if(angka %2 ==1){
        console.log(angka + ' - Santai')
    }
}


//soal 3
for(var i = 1; i<=7; i++){
    console.log("#".repeat(i))
}

//soal 4
var kalimat="saya sangat senang belajar javascript";
var res = kalimat.split(" ");

console.log(res);

//soal 5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
daftarBuah.sort()
for(i=0; i <daftarBuah.length; i++){
    console.log(daftarBuah[i]);
}